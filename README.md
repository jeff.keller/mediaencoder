# Overview

{Endarr} is an R package for monitoring and encoding video files. It is largely a wrapper for the [HandBrakeCLI](https://handbrake.fr/docs/en/latest/cli/command-line-reference.html), a feature-rich video encoding tool.

However, {Endarr} provides a few things that HandBrake does not:

* Keeps track of which video files have already been processed
* Auto-detects new video files and maintains an encoding queue
* [BROKEN] Email notifications of failed jobs
* [TODO] Provides a simple web interface for viewing encoding progress and storage savings
* [TODO] Distributed encoding (use multiple computers to encode video files)

# Installation

1. Install [HandBrake](https://handbrake.fr/) version 1.4.0 or newer.

    > **Note:** If you choose to install HandBrake via your distribution's package manager (rather than Flatpak) be sure to install the `handbrake-cli` package.
    The `handbrake` package by itself isn't sufficient.

2. Install [R version 4.1.2](https://docs.rstudio.com/resources/install-r/).

3. Install `libcurl` using your distribution's package manager.

    * `libcurl4-openssl-dev` (Debian, Ubuntu, etc)
    * `libcurl-devel` (Fedora, CentOS, RHEL)
    * `libcurl_dev` (Solaris)
    
4. Install `libssl` using your distribution's package manager.

    * `libssl-dev` (Debian, Ubuntu, etc)
    * `openssl-devel` (Fedora, CentOS, RHEL)
    * `libssl_dev` (Solaris)

5. Install {endarr} dependencies:

    ```bash
    git clone https://gitlab.com/jeff.keller/endarr.git
    cd endarr
    R -e 'renv::restore()'
    ```

# How it Works

Library scans detect media files and compare their signatures to a signature manifest stored alongside the files (the `.encode-manifest` file by default).
If a file's signature differs from what is in the manifest (or is not present at all), then the file is added to the encoding queue.

Files in the encoding queue are processed by HandBrake using the configured preset.
The default preset encodes video tracks as x265, passes-through all audio tracks, and drops all but English subtitle tracks.

# Usage

## Initialize

For first time usage, initialize using the `init.R` script:

```bash
Rscript init.R
```

This will create a configuration file at `~/.config/R/endarr/config.json` which you should edit to define your media libraries. For example:

_~/.config/R/endarr/config.json_

```txt
...

"libraries": {
  "Movies": "/mnt/Media/Movies",
  "TV Shows": "/mnt/Media/TV Shows"
}

...
```

## Scan Libraries

Scan libraries for files that require processing.
They will be placed in an encoding queue (`~/.config/R/endarr/encode.q` by default).

```bash
Rscript scan_libs.R
```

## Encode Files

Process files in the encoding queue:

```bash
Rscript encode_queued_files.R
```

### HandBrake Presets

To use a preset other than the default preset that ships with this project:

1. [Create a preset](https://handbrake.fr/docs/en/latest/technical/official-presets.html) using the HandBrake GUI.
2. Export your preset to a file.
3. Update the {endarr} `config.json` file to use your custom preset.

    ```txt
    ...
    
    "encode": {
      "preset": "/path/to/preset.json",
      ...
    },
    
    ...
    ```

# Known Issues

* Only available on Linux
